# README

> [Node](https://nodejs.org/fr/), Express, MongoDB

## Back

### Obsolète

```sh
node server
```

### Installation

```sh
npm install --save express
```

### Démarrage

```sh
npm init
npm install
sudo npm install -g nodemon
nodemon server
```

## Front

```sh
ng serve
```

* [Server](http://localhost:3000/)
* [Client](http://localhost:4200/)

## Resources

* [NodeJS (1/6) : Qu'est ce que NodeJS ?](https://www.youtube.com/watch?v=0PA69L88HeI) !yt
